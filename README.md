## PROJECT INFO

TASK MANAGER

## DEVELOPER INFO

**NAME:** VLADISLAV HALMETOV

**E-MAIL:** halmetoff@gmail.com

## SOFTWARE

- JDK 1.8
- MS WINDOWS 10

## HARDWARE

**CPU:** Intel Core i5-4590 or better

**RAM:** 8 GB

**ROM:** 100 MB

## PROGRAM RUN

```bash
java -jar ./task-manager.jar
```

## SCREENSHOTS

https://drive.google.com/drive/folders/1BwHK6M6Lkg_wlNbOpYL-xJmcbbHBQSx4?usp=sharing